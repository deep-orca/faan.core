from setuptools import setup as xsetup, find_packages

xsetup(
    name = 'faan.core',
    version = '0.2.2',
    description = 'faan heart',
    author = '',
    author_email = '',
    url = '',
    namespace_packages = ['faan'],
    install_requires = [
        "SQLAlchemy",
        "Flask",
        "repoze.who",
        #"repoze.what",
        "beaker",
        "mako",
        "redis",
        "psycopg2",
        "webhelpers",
        "PasteScript",
        "zope.interface>=4"
    ],
    packages = find_packages(exclude = ['ez_setup']),
    include_package_data = True,
    zip_safe = False,
    entry_points = """
    [paste.filter_app_factory]
    SQLAHelper = faan.core.X.sessionremover:SessionRemover
    SiteID = faan.core.X.siteid:sitecurrent_middleware
    """
)

