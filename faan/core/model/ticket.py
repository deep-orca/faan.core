import datetime
import time
from sqlalchemy import Column, BigInteger, Integer, Unicode, ForeignKey
from sqlalchemy.orm import relationship, backref, Session
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import case
from faan.core.X import XEnum
from faan.core.model import BaseMixin, meta
from faan.core.model.security import Account
from faan.web.app import account


class _Ticket_State(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.OPEN = 0
        self.RESOLVED = 1


class _Ticket_Category(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.GENERAL = 0
        self.TECHNIC = 1
        self.FINANCIAL = 2


class _Ticket_Urgency(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.LOW = 0
        self.MEDIUM = 1
        self.HIGH = 2
        self.CRITICAL = 3


class Ticket(BaseMixin, meta.Base):
    __tablename__ = 'tickets'
    id = Column(BigInteger, primary_key=True, autoincrement=True)
    #account = Column(BigInteger, nullable=False)

    # account = relationship("Account", uselist=False)    # Account object
    account_id = Column(BigInteger, nullable=False)     # Account ID

    ts_created = Column(BigInteger, nullable=False)
    ts_viewed_owner = Column(BigInteger, nullable=False)
    ts_viewed_support = Column(BigInteger, nullable=False)

    state = Column(Integer, nullable=False)
    category = Column(Integer, nullable=False)
    urgency = Column(Integer, nullable=False, default=0)

    title = Column(Unicode(64), nullable=False)
    message = Column(Unicode(2044), nullable=False)

    # replies = relationship("TicketReply")

    @property
    def replies(self):
        return Session().query(TicketReply).filter(TicketReply.ticket == self.id).all() or []

    @property
    def account(self):
        return Session().query(Account).filter(Account.id == self.account_id).first() or None

    @property
    def new_replies_owner(self):
        """Return True if Ticket have one or many new replies

            If last reply is by supporter
            &&
            If last reply ts_created > ts_viewed_owner

        """
        has_new_replies = new_replies = False  # Default result
        ts_now = int(time.mktime(datetime.datetime.utcnow().timetuple()))

        try:
            last_reply = self.replies[-1]
        except:
            last_reply = None

        if last_reply and \
           last_reply.account_id != self.account_id and \
           last_reply.ts_created > self.ts_viewed_owner:
            has_new_replies = True
            for r in self.replies:
                if r.ts_created > self.ts_viewed_owner and r.account_id != account.id:
                    new_replies += 1

        return has_new_replies, new_replies

    @property
    def new_replies_admin(self):
        """Return True if Ticket have one or many new replies

            If last reply is by supporter
            &&
            If last reply ts_created > ts_viewed_support

        """
        has_new_replies = new_replies = False  # Default result

        replies = self.replies

        if replies:
            for r in replies:
                if r.ts_created > self.ts_viewed_support and r.account_id != account.id:
                    new_replies += 1
                    has_new_replies = True
        elif self.ts_viewed_support == 0:
            has_new_replies = True
            new_replies = 1

        return has_new_replies, new_replies

    State = _Ticket_State()
    Category = _Ticket_Category()
    Urgency = _Ticket_Urgency()


class TicketReply(BaseMixin, meta.Base):
    __tablename__ = 'tickets_reply'
    id = Column(BigInteger, primary_key=True, autoincrement=True)

    # account = relationship("Account", uselist=False)    # Account object
    account_id = Column(BigInteger, nullable=False)     # Account ID

    ticket = Column(BigInteger, ForeignKey("tickets.id"), nullable=False)
    ts_created = Column(BigInteger, nullable=False)
    message = Column(Unicode(2044), nullable=False)

    @property
    def account(self):
        return Session().query(Account).filter(Account.id == self.account_id).first() or None