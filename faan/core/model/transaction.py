from faan.core.model import meta
from faan.core.X import XEnum
from sqlalchemy import Column, Integer, Unicode, DECIMAL, BigInteger
from faan.core.model.basemixin import BaseMixin


class _TransactionTypes(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.MANUAL     = 0
        self.IMPRESSION = 1
        self.PAYSYSTEM  = 2


class _TransactionPaysystems(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.MANUAL     = 0
        self.WEBMONEY   = 1
        self.YANDEX     = 2
        self.QIWI       = 3
        self.PAYPAL     = 4
        self.CARD       = 5


class Transaction(BaseMixin, meta.Base):
    __tablename__ = 'transactions'

    id          = Column(BigInteger,    nullable=False, primary_key=True, autoincrement=True)
    
    ts_spawn    = Column(Integer,       nullable=False)
    type        = Column(Integer,       nullable=False)
    ps          = Column(Integer,       nullable=False, default=0,      server_default="0")
    wallet      = Column(Unicode(255),  nullable=False, default=u"",    server_default="")

    dst_acc     = Column(Integer,       nullable=False, default=0,      server_default="0")
    issuer_acc  = Column(Integer,       nullable=False, default=0,      server_default="0")
    amount      = Column(DECIMAL,       nullable=False, default=0,      server_default="0")

    reference   = Column(Integer,       nullable=False, default=0,      server_default="0")
    comment     = Column(Unicode(255),  nullable=False, default=u"",    server_default="")

    Type        = _TransactionTypes()
    PaySystem   = _TransactionPaysystems()