# coding=utf-8


from faan.core.model import meta, PGArray
from faan.core.X import XEnum
from sqlalchemy import BigInteger, Column, Unicode, Integer
from faan.core.model.basemixin import BaseMixin
from sqlalchemy.sql.expression import text


__author__ = 'limeschnaps'


class _News_Priority(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.LOW = -1
        self.NORMAL = 0
        self.HIGH = 1
        self.CRITICAL = 2


class _News_For_Group(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.ALL = 0
        self.PUB = 1
        self.ADV = 2


class News(BaseMixin, meta.Base):
    __tablename__ = "news"

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    created_by = Column(BigInteger, nullable=False)
    ts_created = Column(BigInteger, nullable=False)
    title = Column(Unicode(100), nullable=False)
    message = Column(Unicode(2044), nullable=False)
    priority = Column(Integer, nullable=False, default=0)
    for_group = Column(Integer, nullable=False, default=0)
    tags = Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))

    Priority = _News_Priority()
    ForGroup = _News_For_Group()