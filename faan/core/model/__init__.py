"""The application's model objects"""
import sqlalchemy as sa
from sqlalchemy import create_engine, orm
from sqlalchemy.dialects.postgresql import ARRAY as PGArray
from sqlalchemy.dialects.postgresql import BIGINT as PGBigInteger

from faan.core.X import XEnum

def init_model(): 
    pass

import meta
from .basemixin import BaseMixin
from .category import Category
