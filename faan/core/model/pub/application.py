from faan.core.model import meta, XEnum, BaseMixin, PGArray
from sqlalchemy import Column, Integer, Unicode, BigInteger, text, DECIMAL
from decimal import Decimal
from faan.core.model.meta import Session
from faan.core.model.pub.zone import Zone
import uuid

class _Application_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.REGULAR = 1


class _Application_Flags(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0


class _Application_States(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.ACTIVE = 1
        self.DISABLED = 2
        self.SUSPENDED = 3


class _Application_Os(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.IOS = 1
        self.ANDROID = 2
        self.WINPHONE = 3
        self.MOBILE_WEB = 4


class _Application_Content_Rating(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = -1
        self.EVERYONE = 0
        self.LOW = 1
        self.MEDIUM = 2
        self.HIGH = 3


class Application(BaseMixin, meta.Base):
    __tablename__ = 'applications'

    id = Column(BigInteger, nullable=False, primary_key=True, autoincrement=True)     #@ReservedAssignment
    uuid        = Column(Unicode(36),       nullable=False, default=lambda: "%s" % uuid.uuid4())
    
    account = Column(BigInteger, nullable=False)
    name = Column(Unicode(64), nullable=False, default=u"", server_default="''")
    store_url = Column(Unicode(255), nullable=False, default=u"", server_default="''")
    icon_url = Column(Unicode(1000), nullable=False, default=u"", server_default="''")
    os = Column(Integer, nullable=False)
    content_rating = Column(Integer, nullable=False, default=0, server_default="0")

    category = Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))
    category_groups = Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))
    target_categories = Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))

    # impressions = Column(Integer, nullable=False, default=0, server_default="0")
    # raw = Column(Integer, nullable=False, default=0, server_default="0")
    # revenue = Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")
    # v4vc_cost = Column(Integer, nullable=False, default=0, server_default="0")

    type = Column(Integer, nullable=False, default=0, server_default="0")      #@ReservedAssignment
    state = Column(Integer, nullable=False, default=0, server_default="0")      #@ReservedAssignment
    flags = Column(Integer, nullable=False, default=0, server_default="0")      #@ReservedAssignment

    Os = _Application_Os()
    Platform = _Application_Os() # duplicate, Os is wrong
    Type = _Application_Types()
    Flag = _Application_Flags()
    State = _Application_States()
    ContentRating = _Application_Content_Rating()


"""
CREATE TABLE "public"."applications" ( 
    "id"                 BIGSERIAL               NOT NULL,
    "account"            BIGINT                  NOT NULL,   
    "name"               CHARACTER VARYING( 64 ) NOT NULL DEFAULT '',
    "os"                 INTEGER                 NOT NULL,  
    "category"           INTEGER                 NOT NULL, 
    "target_categories" INTEGER[]               NOT NULL DEFAULT ARRAY[]::INTEGER[], 
    "type"               INTEGER                 NOT NULL DEFAULT '0',
    "state"              INTEGER                 NOT NULL DEFAULT '0',
    "flags"              INTEGER                 NOT NULL DEFAULT '0',
 PRIMARY KEY ( "id" )
 );
"""
