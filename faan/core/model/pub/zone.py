from faan.core.model import meta, XEnum, BaseMixin
from sqlalchemy import Column, Integer, Unicode, BigInteger, DECIMAL
from decimal import Decimal
import uuid

# class _Zone_Types(XEnum):
#     def __init__(self):
#         XEnum.__init__(self)
#         self.NONE         = 0
#         self.INTERSTITIAL = 1
#         self.V4VC         = 2
from faan.core.model.general.sizes import EnumSizes


class _Zone_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.BANNER = 1
        self.INTERSTITIAL = 2

class _Zone_Device_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.PHONE = 1
        self.TABLET = 2

class _Zone_Flags(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE         = 0

class _Zone_States(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE         = 0
        self.ACTIVE       = 1
        self.DISABLED     = 2

# class _Zone_Sizes(XEnum):
#     def __init__(self):
#         XEnum.__init__(self)
#         self.BANNER             = 0     # 320x50
#         self.LARGE_BANNER       = 1     # 320x100
#         self.MEDIUM_RECTANGLE   = 2     # 300x250
#         self.FULL_BANNER        = 3     # 468x60
#         self.LEADERBOARD        = 4     # 728x90
#         self.SMART_BANNER       = 5

def mydefault():
    return "%s" % uuid.uuid4()


class Zone(BaseMixin, meta.Base):

    __tablename__ = 'zones'
    


    # General
    id                  = Column(BigInteger,        nullable=False, primary_key=True, autoincrement=True)     #@ReservedAssignment
    application         = Column(BigInteger,        nullable=False)
    uuid                = Column(Unicode(36),       nullable=False, default=lambda: "%s" % uuid.uuid4())

    name                = Column(Unicode(64),       nullable=False, default=u"",        server_default=""    )
    type                = Column(Integer,           nullable=False, default=1,          server_default="1"   )      #@ReservedAssignment
    device_type         = Column(Integer,           nullable=False, default=1,          server_default="1")
    session_limit       = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    daily_limit         = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    mod_factor          = Column(Integer,           nullable=False, default=0,          server_default="0"   )

    # Banner
    width               = Column(Integer,           nullable=False, default=0,          server_default="0")
    height              = Column(Integer,           nullable=False, default=0,          server_default="0")

    refresh_rate        = Column(Integer,           nullable=False, default=0,          server_default="0")

    # Interstitial
    show_video          = Column(Integer,           nullable=False, default=0,          server_default="0")
    v4vc_cpv            = Column(Integer,           nullable=False, default=0,          server_default="0")

    # If +video
    min_duration        = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    max_duration        = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    closable            = Column(Integer,           nullable=False, default=0,          server_default="0"   )      # view-time to enable "close" button after


    # Stat
    # impressions         = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    # raw                 = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    # v4vc_cost           = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    # revenue             = Column(DECIMAL,           nullable=False, default=Decimal(0), server_default="0"   )


    state               = Column(Integer,           nullable=False, default=0,          server_default="0"   )      #@ReservedAssignment
    flags               = Column(Integer,           nullable=False, default=0,          server_default="0"   )      #@ReservedAssignment
    
    Type        = _Zone_Types()
    Flag        = _Zone_Flags()
    State       = _Zone_States()
    Size        = EnumSizes()
    DeviceType  = _Zone_Device_Types()


"""
CREATE TABLE "public"."zones" ( 
    "id"                 BIGSERIAL               NOT NULL, 
    "name"               CHARACTER VARYING( 64 ) NOT NULL DEFAULT '',
    "application"        BIGINT                  NOT NULL,  
    "v4vc_cpv"           INTEGER                 NOT NULL, 
    "type"               INTEGER                 NOT NULL DEFAULT '0',
    "state"              INTEGER                 NOT NULL DEFAULT '0',
    "flags"              INTEGER                 NOT NULL DEFAULT '0',
 PRIMARY KEY ( "id" )
 );
"""
