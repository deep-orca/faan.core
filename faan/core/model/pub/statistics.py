import time
from decimal import Decimal
from sqlalchemy import Column, Integer, DECIMAL, BigInteger

from faan.core.model import meta, BaseMixin
from faan.core.model.basemixin import StatMixin
from faan.x.util import try_int, TimeStamp
from faan.core.model.general.geo import Geo


class _StatPresetsHolder(object):
    """
    *@DynamicAttrs*
    """
    def __init__(self):
        self.GROUP_DAILY = ''
        self.GROUP_HOURLY = ''


class PubStat(BaseMixin, StatMixin, meta.Base):

    __tablename__ = 'pub_stats'

    #Compound key
    account         = Column(Integer,    primary_key=True)     # to avoid joining
    application     = Column(Integer,    primary_key=True)     # to avoid joining
    zone            = Column(Integer,    primary_key=True)
    unit            = Column(Integer,    primary_key=True)
    source          = Column(Integer,    primary_key=True)
    ts_spawn        = Column(Integer,    primary_key=True)     # ts hourly
    country         = Column(Integer,    primary_key=True)
    region          = Column(Integer,    primary_key=True)
    city            = Column(Integer,    primary_key=True)

    # Non-unique data
    impressions     = Column(Integer, default=0)
    raw             = Column(Integer, default=0)
    v4vc_cost       = Column(Integer, default=0)
    revenue         = Column(DECIMAL, default=Decimal(0))
    requests        = Column(Integer, default=0)
    clicks          = Column(Integer, default=0)

    Presets = _StatPresetsHolder()

    @staticmethod
    def _ts_current_hour():
        now = TimeStamp.now()
        return now - now % (60 * 60)

    def __init__(self, *args, **kwargs):
        self.ts_spawn    = self._ts_current_hour()
        self.account     = try_int(kwargs.get('account',     0))
        self.application = try_int(kwargs.get('application', 0))
        self.zone        = try_int(kwargs.get('zone',        0))

PubStat.Presets.GROUP_DAILY    = ((PubStat.ts_spawn - time.timezone) - (PubStat.ts_spawn - time.timezone) % (60 * 60 * 24))
PubStat.Presets.GROUP_DAILY_TZ = lambda tz: ((PubStat.ts_spawn + tz) - (PubStat.ts_spawn + tz) % (60 * 60 * 24))
PubStat.Presets.GROUP_HOURLY   = (PubStat.ts_spawn - PubStat.ts_spawn % (60 * 60))

"""
CREATE TABLE "public"."pub_stats" ( 
    "account"         BIGINT  NOT NULL, 
    "application"     BIGINT  NOT NULL, 
    "zone"            BIGINT  NOT NULL, 
    "ts_spawn"        INTEGER NOT NULL, 
    "impressions"     INTEGER NOT NULL DEFAULT '0', 
    "raw"             INTEGER NOT NULL DEFAULT '0', 
    "v4vc_cost"       INTEGER NOT NULL DEFAULT '0', 
    "cost"            NUMERIC NOT NULL DEFAULT '0', 
 PRIMARY KEY ( "account","application","zone","ts_spawn" )
 );
"""

