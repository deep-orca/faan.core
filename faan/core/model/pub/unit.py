from faan.core.model import meta, XEnum, BaseMixin
from sqlalchemy import Column, Integer, Unicode, BigInteger, DECIMAL
from decimal import Decimal
import uuid
from faan.core.model.general.sizes import EnumSizes


class _Unit_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.BANNER         = 1
        self.INTERSTITIAL   = 2
        #self.NATIVE   = 3

class _Unit_Device_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.PHONE          = 1
        self.TABLET         = 2

class _Unit_Flags(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE           = 0

class _Unit_States(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE           = 0
        self.ACTIVE         = 1
        self.DISABLED       = 2

# class _Unit_Sizes(XEnum):
#     def __init__(self):
#         XEnum.__init__(self)
#         self.BANNER             = 1     # 320x50
#         self.LARGE_BANNER       = 2     # 320x100
#         self.MEDIUM_RECTANGLE   = 3     # 300x250
#         self.FULL_BANNER        = 4     # 468x60
#         self.LEADERBOARD        = 5     # 728x90
#         self.SMART_BANNER       = 6




class Unit(BaseMixin, meta.Base):

    __tablename__ = 'units'

    # General
    id                  = Column(BigInteger,        nullable=False, primary_key=True, autoincrement=True)     #@ReservedAssignment
    application         = Column(BigInteger,        nullable=False)
    uuid                = Column(Unicode(36),       nullable=False, default=lambda: "%s" % uuid.uuid4().get_hex()[0:16])
    name                = Column(Unicode(64),       nullable=False, default=u"",        server_default=""    )
    type                = Column(Integer,           nullable=False, default=0,          server_default="0"   )      #@ReservedAssignment
    state               = Column(Integer,           nullable=False, default=0,          server_default="0"   )      #@ReservedAssignment
    flags               = Column(Integer,           nullable=False, default=0,          server_default="0"   )      #@ReservedAssignment
    
    Type        = _Unit_Types()
    Placement   = _Unit_Types() # duplicate, logical reasons
    Flag        = _Unit_Flags()
    State       = _Unit_States()

    # Banner & Interstitial
    device_type         = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    session_limit       = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    daily_limit         = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    

    # Banner
    width               = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    height              = Column(Integer,           nullable=False, default=0,          server_default="0"   )
    refresh_rate        = Column(Integer,           nullable=False, default=0,          server_default="0"   )

    Size        = EnumSizes()
    DeviceType  = _Unit_Device_Types()

    # Interstitial
    mod_factor          = Column(Integer,           nullable=False, default=0,          server_default="0"   )









