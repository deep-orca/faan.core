from faan.core.model.pub.source import Source 
from faan.core.model.pub.application import Application
from faan.core.model.pub.unit import Unit
from faan.core.X.jsonfield import JSONField, PolyID
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy import Integer

# Vidiger sources base classes
class VidigerInterstitialBase(Source):
    """
    Intestitial base class
    """
    media_types = JSONField("media_types", ARRAY(Integer))
    min_duration = JSONField("min_duration", Integer)
    max_duration = JSONField("max_duration", Integer)
    closable = JSONField("closable", Integer)

    def __init__(self):
        super(VidigerInterstitialBase, self).__init__()


class VidigerBannerBase(Source):
    """
    Banner base class
    """
    media_types = JSONField("media_types", ARRAY(Integer))

    def __init__(self):
        super(VidigerBannerBase, self).__init__()



# Intersitial classes
class VidigerIOSInterstitialSource(PolyID(Source.Type.VIDIGER, Application.Platform.IOS, Unit.Placement.INTERSTITIAL), VidigerInterstitialBase):
    def __init__(self):
        super(VidigerIOSInterstitialSource, self).__init__()
        self.poly = self.POLY


class VidigerAndroidInterstitialSource(PolyID(Source.Type.VIDIGER, Application.Platform.ANDROID, Unit.Placement.INTERSTITIAL), VidigerInterstitialBase):
    AD_CLASS = "com.vidiger.sdk.mediation.vidiger.VidigerInterstitialAdapter"
    AD_PLACEMENT = "INTERSTITIAL"

    def __init__(self):
        super(VidigerAndroidInterstitialSource, self).__init__()
        self.poly = self.POLY


class VidigerWinPhoneInterstitialSource(PolyID(Source.Type.VIDIGER, Application.Platform.WINPHONE, Unit.Placement.INTERSTITIAL), VidigerInterstitialBase):
    def __init__(self):
        super(VidigerWinPhoneInterstitialSource, self).__init__()
        self.poly = self.POLY


class VidigerMobileWebInterstitialSource(PolyID(Source.Type.VIDIGER, Application.Platform.MOBILE_WEB, Unit.Placement.INTERSTITIAL), VidigerInterstitialBase):
    AD_CLASS = "com.vidiger.sdk.mediation.vidiger.VidigerInterstitialAdapter"
    AD_PLACEMENT = "INTERSTITIAL"

    def __init__(self):
        super(VidigerMobileWebInterstitialSource, self).__init__()
        self.poly = self.POLY



# Banner classes
class VidigerIOSBannerSource(PolyID(Source.Type.VIDIGER, Application.Platform.IOS, Unit.Placement.BANNER), VidigerInterstitialBase):
    def __init__(self):
        super(VidigerIOSBannerSource, self).__init__()
        self.poly = self.POLY


class VidigerAndroidBannerSource(PolyID(Source.Type.VIDIGER, Application.Platform.ANDROID, Unit.Placement.BANNER), VidigerInterstitialBase):
    AD_CLASS = "window.vdgr.adapters.vidiger"
    AD_PLACEMENT = "BANNER"

    def __init__(self):
        super(VidigerAndroidBannerSource, self).__init__()
        print self.POLY
        self.poly = self.POLY


class VidigerWinPhoneBannerSource(PolyID(Source.Type.VIDIGER, Application.Platform.WINPHONE, Unit.Placement.BANNER), VidigerInterstitialBase):
    def __init__(self):
        super(VidigerWinPhoneBannerSource, self).__init__()
        self.poly = self.POLY


class VidigerMobileWebBannerSource(PolyID(Source.Type.VIDIGER, Application.Platform.MOBILE_WEB, Unit.Placement.BANNER), VidigerInterstitialBase):
    AD_CLASS = "window.vdgr.adapters.vidiger"
    AD_PLACEMENT = "BANNER"

    def __init__(self):
        super(VidigerMobileWebBannerSource, self).__init__()
        self.poly = self.POLY



# Deprecated

# class VidigerAndroidInterstitialSource(PolyID(Source.Type.VIDIGER, Application.Platform.ANDROID, Unit.Placement.INTERSTITIAL), Source):
#     media_types  = JSONField("media_types",     ARRAY(Integer))
#     min_duration = JSONField("min_duration",    Integer)
#     max_duration = JSONField("max_duration",    Integer)
#     closable     = JSONField("closable",        Integer)
#
#     AD_CLASS        = "com.vidiger.sdk.mediation.vidiger.VidigerInterstitialAdapter"
#     AD_PLACEMENT    = "INTERSTITIAL"
     
    

# class VidigerIOSInterstitialSource(PolyID(Source.Type.VIDIGER, Application.Platform.IOS, Unit.Placement.INTERSTITIAL), Source):
#     media_types  = JSONField("media_types",     ARRAY(Integer))
#     min_duration = JSONField("min_duration",    Integer)
#     max_duration = JSONField("max_duration",    Integer)
#     closable     = JSONField("closable",        Integer)
 
    

# class VidigerWinPhoneInterstitialSource(PolyID(Source.Type.VIDIGER, Application.Platform.WINPHONE, Unit.Placement.INTERSTITIAL), Source):
#     media_types  = JSONField("media_types",     ARRAY(Integer))
#     min_duration = JSONField("min_duration",    Integer)
#     max_duration = JSONField("max_duration",    Integer)
#     closable     = JSONField("closable",        Integer)
    

# class VidigerMobileWebInterstitialSource(PolyID(Source.Type.VIDIGER, Application.Platform.MOBILE_WEB, Unit.Placement.INTERSTITIAL), Source):
#     media_types  = JSONField("media_types",     ARRAY(Integer))
#     min_duration = JSONField("min_duration",    Integer)
#     max_duration = JSONField("max_duration",    Integer)
#     closable     = JSONField("closable",        Integer)
#
#     AD_CLASS        = "com.vidiger.sdk.mediation.vidiger.VidigerInterstitialAdapter"
#     AD_PLACEMENT    = "INTERSTITIAL"
    


# class VidigerAndroidBannerSource(PolyID(Source.Type.VIDIGER, Application.Platform.ANDROID, Unit.Placement.BANNER), Source):
#     media_types  = JSONField("media_types", ARRAY(Integer))
#
#     AD_CLASS        = "com.vidiger.sdk.mediation.vidiger.VidigerBannerAdapter"
#     AD_PLACEMENT    = "BANNER"
#
#
# class VidigerMobileWebBannerSource(PolyID(Source.Type.VIDIGER, Application.Platform.MOBILE_WEB, Unit.Placement.BANNER), Source):
#     media_types  = JSONField("media_types", ARRAY(Integer))
#
#     AD_CLASS        = "window.vdgr.adapters.vidiger"
#     AD_PLACEMENT    = "BANNER"



# class VidigerIOSBannerSource(PolyID(Source.Type.VIDIGER, Application.Platform.IOS, Unit.Placement.BANNER), Source):
#     media_types  = JSONField("media_types", ARRAY(Integer))


# class VidigerWinPhoneBannerSource(PolyID(Source.Type.VIDIGER, Application.Platform.WINPHONE, Unit.Placement.BANNER), Source):
#     media_types  = JSONField("media_types", ARRAY(Integer))