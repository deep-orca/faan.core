from faan.core.model.pub.source import Source
from faan.core.model.pub.application import Application
from faan.core.model.pub.unit import Unit
from faan.core.X.jsonfield import JSONField, PolyID
from sqlalchemy import Unicode


class AdMobSourceBase(Source):
    admob_unit = JSONField("unit", Unicode)

    def __init__(self):
        super(AdMobSourceBase, self).__init__()

class AdMobAndroidBannerSource(PolyID(Source.Type.ADMOB, Application.Platform.ANDROID, Unit.Placement.BANNER), AdMobSourceBase):
    AD_CLASS        = "com.vidiger.sdk.mediation.admob.AdMobBannerAdapter"
    AD_PLACEMENT    = "BANNER"

    def __init__(self):
        super(AdMobAndroidBannerSource, self).__init__()
        self.poly = self.POLY


class AdMobAndroidInterstitialSource(PolyID(Source.Type.ADMOB, Application.Platform.ANDROID, Unit.Placement.INTERSTITIAL), AdMobSourceBase):
    AD_CLASS        = "com.vidiger.sdk.mediation.admob.AdMobInterstitialAdapter"
    AD_PLACEMENT    = "INTERSTITIAL"

    def __init__(self):
        super(AdMobAndroidInterstitialSource, self).__init__()
        self.poly = self.POLY


class AdMobIOSBannerSource(PolyID(Source.Type.ADMOB, Application.Platform.IOS, Unit.Placement.BANNER), AdMobSourceBase):
    def __init__(self):
        super(AdMobIOSBannerSource, self).__init__()
        self.poly = self.POLY


class AdMobIOSInterstitialSource(PolyID(Source.Type.ADMOB, Application.Platform.IOS, Unit.Placement.INTERSTITIAL), AdMobSourceBase):
    def __init__(self):
        super(AdMobIOSInterstitialSource, self).__init__()
        self.poly = self.POLY