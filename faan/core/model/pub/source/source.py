from faan.core.model import meta, XEnum, BaseMixin
from sqlalchemy import Column, Integer, Unicode, BigInteger, DECIMAL
from decimal import Decimal
from sqlalchemy.dialects.postgresql import JSON
from faan.core.X.jsonfield import MutableManually
from faan.core.model.meta import Session
from sqlalchemy.orm.util import with_polymorphic


class _Source_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.VIDIGER              = 1
        
        self.CUSTOM               = 100
        self.ADMOB                = 101
        self.ADFOX                = 102
        
        self.BACKFILL             = 200

SOURCE_TYPES = _Source_Types()
 

class _Source_Flags(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE         = 0

class _Source_States(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE         = 0
        self.ACTIVE       = 1
        self.DISABLED     = 2



class Source(BaseMixin, meta.Base):
    
    __tablename__ = 'sources'

    id         = Column(BigInteger,                         nullable=False, primary_key=True, autoincrement=True)           #@ReservedAssignment

    unit       = Column(BigInteger,                         nullable=False)
    name       = Column(Unicode(64),                        nullable=False)
    ecpm       = Column(DECIMAL,                            nullable=False, default=Decimal(0), server_default="0"   )      # effective CPM for mediated 

    parameters = Column(MutableManually.as_mutable(JSON),   nullable=False, default={ }                              )      #
    
    poly       = Column(Integer,                            nullable=False)      #@ReservedAssignment
    # poly       = Column(Integer,                            nullable=False, default=0,          server_default="0"   )      #@ReservedAssignment
    type       = Column(Integer,                            nullable=False, default=0,          server_default="0"   )      #@ReservedAssignment
    
    state      = Column(Integer,                            nullable=False, default=0,          server_default="0"   )      #@ReservedAssignment
    flags      = Column(Integer,                            nullable=False, default=0,          server_default="0"   )      #@ReservedAssignment
    
    
    Type       = _Source_Types()
    Flag       = _Source_Flags()
    State      = _Source_States()
    

    __mapper_args__ = { 'polymorphic_on'    : poly }

#     
#     @staticmethod
#     def QWithAppUnit(app, unit):
#         polyq = with_polymorphic(Source, "*") # CLASS_MAP[(app.os, unit.type)])
#         return Session.query(polyq)
#     
    

    





