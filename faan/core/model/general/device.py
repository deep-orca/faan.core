from sqlalchemy import Column, Integer, Unicode, UnicodeText, BigInteger
from faan.core.model import meta, BaseMixin
from faan.core.X.enum import XEnum
from sqlalchemy import PickleType

class _Device_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.REGULAR     = 1

class Device(BaseMixin, meta.Base):

    __tablename__ = 'devices'

    id              = Column(BigInteger,        nullable=False, primary_key=True)
    
    properties      = Column(PickleType,        nullable=False, default={}) 
    timestamp       = Column(Integer,           nullable=False, default=0)

    
    Type = _Device_Types()
