from decimal import Decimal
from sqlalchemy import Column, Integer, Unicode, DECIMAL
from faan.core.model import meta, BaseMixin



class GeoCountry(BaseMixin, meta.Base):

    __tablename__ = 'geo_countries'

    id              = Column(Integer,        nullable=False, primary_key=True)
    name            = Column(Unicode,        nullable=False, default=u"", server_default=""    )
    code            = Column(Unicode(2),     nullable=False, default=u"", server_default=""    )
    multiplier      = Column(DECIMAL,        nullable=False, default=Decimal(1))
    
    en_name         = Column(Unicode,        nullable=False, default=u"", server_default=""    )
    state           = Column(Integer,        nullable=False, default=0)
    
    
    def __str__(self):
        return "<Country [%s / %s] : %s>" % (self.id, self.code, self.name)
    
    def __repr__(self, *args, **kwargs):
        return self.__str__()


# class GeoState(BaseMixin, meta.Base):
#  
#     __tablename__ = 'geo_states'
#  
#     id              = Column(Integer,        nullable=False, primary_key=True)
#     country         = Column(Integer,        nullable=False)
#     name            = Column(Unicode,        nullable=False, default=u"", server_default=""    )

class GeoRegion(BaseMixin, meta.Base):

    __tablename__ = 'geo_regions'

    id              = Column(Integer,        nullable=False, primary_key=True)
    country         = Column(Integer,        nullable=False)
    name            = Column(Unicode,        nullable=False, default=u"", server_default=""    )
    multiplier      = Column(DECIMAL,        nullable=False, default=Decimal(1))

    en_name         = Column(Unicode,        nullable=False, default=u"", server_default=""    )
    state           = Column(Integer,        nullable=False, default=0)


    def __str__(self):
        return "<Region [%s @ %s] : %s>" % (self.id, self.country, self.name)

    def __repr__(self, *args, **kwargs):
        return self.__str__()


class GeoCity(BaseMixin, meta.Base):

    __tablename__ = 'geo_cities'

    id              = Column(Integer,        nullable=False, primary_key=True)
    region          = Column(Integer,        nullable=False)
    name            = Column(Unicode,        nullable=False, default=u"", server_default=""    )
    ipgb_code       = Column(Unicode,        nullable=False, default=u"", server_default=""    )

    gm_en_code      = Column(Unicode,        nullable=False, default=u"", server_default=""    )
    gm_ru_code      = Column(Unicode,        nullable=False, default=u"", server_default=""    )

    en_name         = Column(Unicode,        nullable=False, default=u"", server_default=""    )
    state           = Column(Integer,        nullable=False, default=0)
    
    multiplier = Column(DECIMAL, nullable=False, default=Decimal(1))

    def __str__(self):
        return "<City [%s @ %s] : %s>" % (self.id, self.region, self.name)

    def __repr__(self, *args, **kwargs):
        return self.__str__()


class Geo(object):                              # COUNTRY > STATE > REGION > CITY
    CITY_MASK       = 0x000000000000FFFF
    REGION_MASK     = 0x00000000FFFF0000
#    STATE_MASK      = 0x00000000FF000000
    COUNTRY_MASK    = 0x0000FFFF00000000
    
    CITY_OFFSET     = 0
    REGION_OFFSET   = 16
#    STATE_OFFSET    = 24
    COUNTRY_OFFSET  = 32
    
    CITY_ALL        = CITY_MASK    >> CITY_OFFSET
    REGION_ALL      = REGION_MASK  >> REGION_OFFSET
#    STATE_ALL       = STATE_MASK   >> STATE_OFFSET
    COUNTRY_ALL     = COUNTRY_MASK >> COUNTRY_OFFSET
    
    def __init__(self, intval  = None
                     , country = None
#                     , state   = None
                     , region  = None
                     , city    = None):
        
        self.value = intval or 0
        
        if country is not None: self.value |= country << Geo.COUNTRY_OFFSET
#        if state   is not None: self.value |= state   << Geo.STATE_OFFSET
        if region  is not None: self.value |= region  << Geo.REGION_OFFSET
        if city    is not None: self.value |= city    << Geo.CITY_OFFSET
            
    
    def city(self):
        return (self.value & Geo.CITY_MASK)     >> Geo.CITY_OFFSET

    def region(self):
        return (self.value & Geo.REGION_MASK)   >> Geo.REGION_OFFSET

#    def state(self):
#        return (self.value & Geo.STATE_MASK)    >> Geo.STATE_OFFSET
    
    def country(self):
        return (self.value & Geo.COUNTRY_MASK)  >> Geo.COUNTRY_OFFSET
    
    
    def __str__(self, *args, **kwargs):
        if getattr(self, 'value', None) is None:
            return "<Geo : NONE>"
        else:
            return "<Geo # %s:%s:%s>"  % ( "ALL" if self.country()  == Geo.COUNTRY_ALL  else self.country()
                                         , "ALL" if self.region()   == Geo.REGION_ALL   else self.region()
                                         , "ALL" if self.city()     == Geo.CITY_ALL     else self.city() 
                                         )
    
    def __repr__(self, *args, **kwargs):
        return self.__str__()
    
    













