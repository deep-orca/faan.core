from sqlalchemy import Column, Integer, Unicode, BigInteger
from faan.core.model import meta, BaseMixin
from faan.core.X.enum import XEnum

class _Identity_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.ANDROID_ID       = 1
        self.ANDROID_SERIAL   = 2
        self.IOS_IDFV         = 3
        
        self.COOKIE_TAG       = 200

class DeviceIdentity(BaseMixin, meta.Base):

    __tablename__ = 'device_identities'

    id              = Column(BigInteger,        nullable=False, primary_key=True)
    
    device          = Column(BigInteger,        nullable=False)
    name            = Column(Unicode(64),       nullable=False, default=u'')
    value           = Column(Unicode(255),      nullable=False, default=u'') 
    
    type            = Column(Integer,           nullable=False, default=0)
    timestamp       = Column(Integer,           nullable=False, default=0)

    
    Type = _Identity_Types()