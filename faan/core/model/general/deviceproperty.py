
from sqlalchemy import Column, Integer, Unicode, UnicodeText, BigInteger
from faan.core.model import meta, BaseMixin
from faan.core.X.enum import XEnum

class DeviceProperty(BaseMixin, meta.Base):

    __tablename__ = 'device_properties'

    id              = Column(BigInteger,        nullable=False, primary_key=True)
    
    device          = Column(BigInteger,        nullable=False)    
    name            = Column(Unicode(64),       nullable=False, default=u'')
    value           = Column(Unicode(255),      nullable=False, default=u'')    # probably UnicodeText fits better
    
    timestamp       = Column(Integer,           nullable=False, default=0)      # insertion timestamp    

    