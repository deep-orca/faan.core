# coding=utf-8
from faan.core.X import XEnum

__author__ = 'limeschnaps'


class Size(object):
    FILL = 0

    def __init__(self, width, height, custom=False):
        self.width = width
        self.height = height
        self.custom = custom

    def __repr__(self):
        return self.width, self.height, self.custom

    def __call__(self, *args, **kwargs):
        return tuple([self.width, self.height])


class EnumSizes(XEnum):
    def __init__(self):
        super(EnumSizes, self).__init__()
        self.BANNER             = Size(320,     50                  )     # 320x50
        self.LARGE_BANNER       = Size(320,     100                 )     # 320x100
        self.MEDIUM_RECTANGLE   = Size(300,     250                 )     # 300x250
        self.FULL_BANNER        = Size(468,     60                  )     # 468x60
        self.LEADERBOARD        = Size(728,     90                  )     # 728x90
        self.FULLSCREEN_PHONE   = Size(320,     480                 )
        self.FULLSCREEN_TABLET  = Size(1024,    768                 )
        self.CUSTOM             = Size("",     "",   custom=True    )     # Custom size
        # self.SMART_BANNER       = Size(Size.FILL, Size.FILL)  # For later use