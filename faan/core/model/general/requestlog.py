from sqlalchemy import Column, Integer, Unicode, UnicodeText, BigInteger
from faan.core.model import meta, BaseMixin, PGArray
from faan.core.X.enum import XEnum

class RequestLog(BaseMixin, meta.Base):

    __tablename__ = 'request_log'

    id              = Column(BigInteger,        nullable=False, primary_key=True)
    
    device          = Column(BigInteger,        nullable=False, default=0)
    application     = Column(BigInteger,        nullable=False, default=0)
    
    platform        = Column(Integer,           nullable=False, default=0)
    devclass        = Column(Integer,           nullable=False, default=0)

    
    country         = Column(Integer,           nullable=False, default=0)
    region          = Column(Integer,           nullable=False, default=0)
    city            = Column(Integer,           nullable=False, default=0)
    
#    type            = Column(Integer,           nullable=False, default=0)
    timestamp       = Column(Integer,           nullable=False, default=0)
    
