from sqlalchemy import Column, Integer, Unicode, UnicodeText, BigInteger
from faan.core.model import meta, BaseMixin
from faan.core.X.enum import XEnum

class _SDKLog_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.EXCEPTION   = 1
        self.ERROR       = 2
        self.DEBUG       = 3

class SDKLog(BaseMixin, meta.Base):

    __tablename__ = 'sdk_log'

    id              = Column(BigInteger,        nullable=False, primary_key=True)
    
    identities      = Column(Unicode,           nullable=False, default=u"", server_default=""    )
    
    application     = Column(BigInteger,        nullable=False, default=0)                  # if available
    zone            = Column(BigInteger,        nullable=False, default=0)
    campaign        = Column(BigInteger,        nullable=False, default=0)
    media           = Column(BigInteger,        nullable=False, default=0)
    
    platform        = Column(Integer,           nullable=False, default=0)
    devclass        = Column(Integer,           nullable=False, default=0)
    geo             = Column(BigInteger,        nullable=False, default=0)
    
    sdkv            = Column(Integer,           nullable=False, default=0)
    
    type            = Column(Integer,           nullable=False, default=0)
    timestamp       = Column(Integer,           nullable=False, default=0)
    
    message         = Column(UnicodeText,       nullable=False, default=u"", server_default=""    )
    context         = Column(UnicodeText,       nullable=False, default=u"", server_default=""    )

    
    Type = _SDKLog_Types()
