from faan.core.model import meta, XEnum, BaseMixin
from sqlalchemy import Column, Integer, Unicode, BigInteger, Text
from faan.core.model.general.sizes import EnumSizes
from flask.globals import current_app


class _Media_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.BANNER         = 1
        self.VIDEO          = 2
        self.MRAID          = 3

class _Media_Flags(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE           = 0
        self.CONVERTED      = 1 << 0

class _Media_States(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE           = 0
        self.ACTIVE         = 1
        self.DISABLED       = 2
        self.SUSPENDED      = 3

class _Media_Click_Actions(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE           = 0
        self.CALL           = 1
        self.SMS            = 2
        self.URL            = 3
        self.VIDEO          = 4

class _Media_End_Actions(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE           = 0
        self.ENDCARD        = 1

class _Media_Uri_Targets(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.INAPP          = 1
        self.STANDALONE     = 2

class _Media_Placements(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.BANNER         = 1
        self.INTERSTITIAL   = 2

class _Media_MRAID_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.FILE           = 1
        self.URL            = 2
        self.HTML           = 3


class Media(BaseMixin, meta.Base):

    __tablename__ = 'medias'
    
    id                      = Column(BigInteger,        nullable=False, primary_key=True, autoincrement=True)       #@ReservedAssignment

    placement               = Column(Integer,           nullable=False, default=1,          server_default="1")

    name                    = Column(Unicode(64),       nullable=False, default=u"",        server_default="")
    campaign                = Column(BigInteger,        nullable=False)
    
    uri                     = Column(Unicode(255),      nullable=False, default=u"",        server_default="")
    uri_target              = Column(Integer,           nullable=False, default=1,          server_default="1")

    video                   = Column(Unicode(255),      nullable=False, default=u"",        server_default="")
    endcard                 = Column(Unicode(255),      nullable=False, default=u"",        server_default="")
    overlay                 = Column(Unicode(255),      nullable=False, default=u"",        server_default="")
    mraid_type              = Column(Integer,           nullable=False, default=1,          server_default="1")
    mraid_file              = Column(Unicode(255),      nullable=False, default=u"",        server_default="")
    mraid_url               = Column(Unicode(255),      nullable=False, default=u"",        server_default="")
    mraid_html              = Column(Text(),            nullable=False, default=u"",        server_default="")
    banner                  = Column(Unicode(255),      nullable=False, default=u"",        server_default="")
    
    banner_size             = Column(Integer,           nullable=False, default=0,          server_default="0")
    width                   = Column(Integer,           nullable=False, default=0,          server_default="0")
    height                  = Column(Integer,           nullable=False, default=0,          server_default="0")

    phone_number            = Column(Unicode(20),       nullable=False, default=u"",        server_default="")
    action_click            = Column(Integer,           nullable=False, default=0,          server_default="0")
    action_end              = Column(Integer,           nullable=False, default=0,          server_default="0")

    duration                = Column(Integer,           nullable=False, default=0,          server_default="0")
    closable                = Column(Integer,           nullable=False, default=0,          server_default="0")     # view-time to enable "close" button after
    
    rank_impressions        = Column(BigInteger,        nullable=False, default=0,          server_default="0")
    rank_cost               = Column(BigInteger,        nullable=False, default=0,          server_default="0")     # in 1^(-RANK_PRECISION) cents 
    
    daily_limit             = Column(Integer,           nullable=False, default=0,          server_default="0")
    session_limit           = Column(Integer,           nullable=False, default=0,          server_default="0")

    type                    = Column(Integer,           nullable=False, default=1,          server_default="1")     #@ReservedAssignment
    state                   = Column(Integer,           nullable=False, default=0,          server_default="0")     #@ReservedAssignment
    flags                   = Column(Integer,           nullable=False, default=0,          server_default="0")     #@ReservedAssignment
    
    
    Type                    = _Media_Types()
    Flag                    = _Media_Flags()
    State                   = _Media_States()
    # BannerSizes             = _Media_Banner_Sizes()
    BannerSizes             = EnumSizes()
    ClickActions            = _Media_Click_Actions()
    EndActions              = _Media_End_Actions()
    UriTargets              = _Media_Uri_Targets()
    Placement               = _Media_Placements()
    MRAID_Type              = _Media_MRAID_Types()

    @staticmethod
    def ShiftRanks():
        Media.Update({ Media.rank_cost         : Media.rank_cost / 2               # diminish ranking values
                     , Media.rank_impressions  : Media.rank_impressions / 2 
                     }
                     , None)

    def applyRankDeltas(self, cost, impressions):
        #current_app.logger.debug("cost: [%s] rc_delta: [%s]", cost, int(cost * 10**(Media.RANK_COST_PRECISION + 2)))
        Media.Update({ Media.rank_impressions : Media.rank_impressions + impressions
                     , Media.rank_cost        : Media.rank_cost        + int(cost * 10**(Media.RANK_COST_PRECISION + 2))
                     } 
                     , id=self.id)


    RANK_COST_PRECISION  = 2
    RANK_CLAUSE          = staticmethod(lambda ecpm, sampling: Media.rank_cost + int(ecpm * 10**(Media.RANK_COST_PRECISION + sampling + 2)) / (Media.rank_impressions + 10**(sampling + 3)))











