from faan.core.model import meta, XEnum, BaseMixin, PGArray
from sqlalchemy import Column, Integer, Unicode, BigInteger, DECIMAL
from decimal import Decimal
from sqlalchemy.sql.expression import text


class _Campaign_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.REGULAR = 1


class _Campaign_Flags(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0


class _Campaign_States(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.ACTIVE = 1
        self.DISABLED = 2


class _Campaign_Targetings(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.GEO = 1 << 0
        self.CATEGORY = 1 << 1
        self.PLATFORM = 1 << 2
        self.DEVCLASS = 1 << 3
        self.CONTENT_RATING = 1 << 4
        self.AGE = 1 << 5
        self.GENDER = 1 << 6


class _Campaign_DevClasses(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.PHONE = 1
        self.TABLET = 2


class _Campaign_ContentRating(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.LOW = 1
        self.MEDIUM = 2
        self.HIGH = 3


class _Campaign_Targeting_Genders(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.MALE = 1
        self.FEMALE = 2


class _Campaign_Targeting_Ages(XEnum):
    def __init__(self):
        super(_Campaign_Targeting_Ages, self).__init__()
        self.AGE_0_12       = 1
        self.AGE_12_18      = 2
        self.AGE_18_24      = 3
        self.AGE_24_32      = 4
        self.AGE_32_40      = 5
        self.AGE_40_55      = 6
        self.AGE_55         = 7


class _Campaign_Payment_Types(XEnum):
    def __init__(self):
        super(_Campaign_Payment_Types, self).__init__()
        self.CPM = 1 # Cost per 1000
        self.CPC = 2 # Cost per click
        self.CPA = 3 # Cost per action


class Campaign(BaseMixin, meta.Base):
    __tablename__ = 'campaigns'

    id =                        Column(BigInteger, nullable=False, primary_key=True, autoincrement=True)     #@ReservedAssignment

    account =                   Column(BigInteger, nullable=False)
    name =                      Column(Unicode(64), nullable=False, default=u"", server_default="")

    ts_start =                  Column(Integer, nullable=False, default=0, server_default="0")
    ts_end =                    Column(Integer, nullable=False, default=0, server_default="0")

    # limit_total = Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")
    # limit_daily = Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")

    limit_cost_total =          Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")
    limit_cost_daily =          Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")
    limit_imp_total =           Column(Integer, nullable=False, default=Decimal(0), server_default="0")
    limit_imp_daily =           Column(Integer, nullable=False, default=Decimal(0), server_default="0")

    reach_cost_total =          Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")
    reach_cost_daily =          Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")
    reach_imp_total =           Column(Integer, nullable=False, default=Decimal(0), server_default="0")
    reach_imp_daily =           Column(Integer, nullable=False, default=Decimal(0), server_default="0")

    category =                  Column(Integer, nullable=False)
    targeting =                 Column(Integer, nullable=False, default=0, server_default="0")
    target_categories =         Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))
    target_categories_groups =  Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))
    target_platforms =          Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))
    target_devclasses =         Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))
    target_content_rating =     Column(PGArray(Integer), nullable=False, default=[],
                                   server_default=text("ARRAY[]::INTEGER[]"))
    target_geo =                Column(PGArray(BigInteger), nullable=False, default=[], server_default=text("ARRAY[]::BIGINT[]"))
    target_gender =             Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))
    target_age =                Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))
    target_applications =       Column(PGArray(Integer), nullable=False, default=[], server_default=text("ARRAY[]::INTEGER[]"))

    bid =                       Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")
    payment_type =              Column(Integer, nullable=False, default=1, server_default="1")
    # total_cost_limit = Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")

    impressions =               Column(Integer, nullable=False, default=0, server_default="0")
    raw =                       Column(Integer, nullable=False, default=0, server_default="0")
    clicks =                    Column(Integer, nullable=False, default=0, server_default="0")
    cost =                      Column(DECIMAL, nullable=False, default=Decimal(0), server_default="0")
    #
    # overlays = Column(Integer, default=0)               # billed usage counter
    # endcards = Column(Integer, default=0)               # billed usage counter
    # overlay_cost = Column(DECIMAL, default=Decimal(0))
    # endcard_cost = Column(DECIMAL, default=Decimal(0))

    type =                      Column(Integer, nullable=False, default=0, server_default="0")      #@ReservedAssignment
    state =                     Column(Integer, nullable=False, default=0, server_default="0")      #@ReservedAssignment
    flags =                     Column(Integer, nullable=False, default=0, server_default="0")      #@ReservedAssignment

    DevClass =                  _Campaign_DevClasses()
    Targeting =                 _Campaign_Targetings()
    TargetingGender =           _Campaign_Targeting_Genders()
    TargetingAge =              _Campaign_Targeting_Ages()
    PaymentType =               _Campaign_Payment_Types()

    Type =                      _Campaign_Types()
    Flag =                      _Campaign_Flags()
    State =                     _Campaign_States()

    @staticmethod
    def ResetDailyReach():
        Campaign.Update({ Campaign.reach_cost_daily : Decimal(0)                   # reset daily reach for campaigns
                        , Campaign.reach_imp_daily  : 0 
                        }
                        , None)


    
    def accountImpression(self, impression):
        Campaign.Filter(id=self.id).update({ Campaign.reach_imp_total : Campaign.reach_imp_total + impression
                                           , Campaign.reach_imp_daily : Campaign.reach_imp_daily + impression })

    def accountCost(self, cost):
        Campaign.Filter(id=self.id).update({ Campaign.reach_cost_total : Campaign.reach_cost_total + cost
                                           , Campaign.reach_cost_daily : Campaign.reach_cost_daily + cost })



"""
CREATE TABLE "public"."campagins" ( 
    "id" Bigserial NOT NULL, 
    "account" BIGINT NOT NULL,
    "name" CHARACTER VARYING( 64 ) DEFAULT '' NOT NULL, 
    "category" INTEGER NOT NULL, 
    "target_categories" INTEGER[] NOT NULL DEFAULT ARRAY[]::INTEGER[], 
    "total_cost_limit" NUMERIC DEFAULT '0' NOT NULL, 
    "type" INTEGER DEFAULT '0' NOT NULL, 
    "state" INTEGER DEFAULT '0' NOT NULL, 
    "flags" INTEGER DEFAULT '0' NOT NULL,
 PRIMARY KEY ( "id" )
 );
"""

