import time
from decimal import Decimal
from sqlalchemy import Column, Integer, DECIMAL, BigInteger

from faan.core.model import meta, BaseMixin
from faan.core.model.basemixin import StatMixin
from faan.x.util import try_int, TimeStamp
from faan.core.model.general.geo import Geo


class _StatPresetsHolder(object):
    """
    *@DynamicAttrs*
    """
    def __init__(self):
        self.GROUP_DAILY    = ''
        self.GROUP_DAILY_TZ = ''
        self.GROUP_HOURLY   = ''


class AdvStat(BaseMixin, StatMixin, meta.Base):

    __tablename__ = 'adv_stats'

    #Compound key
    account         = Column(BigInteger, primary_key=True)     # to avoid joining
    campaign        = Column(BigInteger, primary_key=True)     # to avoid joining
    media           = Column(BigInteger, primary_key=True)
    ts_spawn        = Column(Integer,    primary_key=True)     # ts hourly
    country         = Column(Integer,    primary_key=True)
    region          = Column(Integer,    primary_key=True)
    city            = Column(Integer,    primary_key=True)
    
    # Non-unique data

    impressions     = Column(Integer, default=0)               # billed impressions
    raw             = Column(Integer, default=0)               # all impressions (includes incomplete thus unbilled)
    clicks          = Column(Integer, default=0)
    
    cost            = Column(DECIMAL, default=Decimal(0))      # includes other *_cost's 

    overlays        = Column(Integer, default=0)               # billed usage counter
    endcards        = Column(Integer, default=0)               # billed usage counter
    overlay_cost    = Column(DECIMAL, default=Decimal(0))
    endcard_cost    = Column(DECIMAL, default=Decimal(0))

    Presets = _StatPresetsHolder()

    @staticmethod
    def ts_current_hour():
        now = TimeStamp.now()
        return now - now % (60 * 60)

    def __init__(self, *args, **kwargs):
        self.ts_spawn   = self.ts_current_hour()
        self.account    = try_int(kwargs.get('account',  0))
        self.campaign   = try_int(kwargs.get('campaign', 0))
        self.media      = try_int(kwargs.get('media',    0))

AdvStat.Presets.GROUP_DAILY    = ((AdvStat.ts_spawn - time.timezone) - (AdvStat.ts_spawn - time.timezone) % (60 * 60 * 24))
AdvStat.Presets.GROUP_DAILY_TZ = lambda tz: ((AdvStat.ts_spawn - tz) - (AdvStat.ts_spawn - tz) % (60 * 60 * 24))
AdvStat.Presets.GROUP_HOURLY   = (AdvStat.ts_spawn - AdvStat.ts_spawn % (60 * 60))


"""
CREATE TABLE "public"."adv_stats" ( 
    "account"         BIGINT  NOT NULL, 
    "campaign"        BIGINT  NOT NULL, 
    "media"           BIGINT  NOT NULL, 
    "ts_spawn"        INTEGER NOT NULL, 
    "impressions"     INTEGER NOT NULL DEFAULT '0', 
    "raw"             INTEGER NOT NULL DEFAULT '0', 
    "clicks"          INTEGER NOT NULL DEFAULT '0', 
    "cost"            NUMERIC NOT NULL DEFAULT '0', 
 PRIMARY KEY ( "account","campaign","media","ts_spawn" )
 );
"""
