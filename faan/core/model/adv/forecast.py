from sqlalchemy import Column, Integer

from faan.core.model import meta, BaseMixin
from faan.core.model.basemixin import StatMixin
from faan.x.util import TimeStamp



class ForecastStat(BaseMixin, StatMixin, meta.Base):

    __tablename__ = 'adv_forecast'

    #Compound key
    category        = Column(Integer, primary_key=True, nullable=False)
    content_rating  = Column(Integer, primary_key=True, nullable=False)
    platform        = Column(Integer, primary_key=True, nullable=False)
    devclass        = Column(Integer, primary_key=True, nullable=False)
    ts_spawn        = Column(Integer, primary_key=True, nullable=False)
    cost_index      = Column(Integer, primary_key=True, nullable=False)
    geo             = Column(Integer, primary_key=True, nullable=False)
    #cost_index      = Column(Integer, primary_key=True, nullable=False)

    # Non-unique data
    impressions     = Column(Integer, nullable=False, default=0)

    @staticmethod
    def ts_current_day():
        now = TimeStamp.now()
        return now - now % (60 * 60)

    def __init__(self, *args, **kwargs):
        self.ts_spawn   = self.ts_current_day()


