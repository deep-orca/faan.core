# coding=utf-8

from sqlalchemy import (
    Column,
    Unicode,
    BigInteger
)

from faan.core.model import meta
from faan.core.model.basemixin import BaseMixin


class Tag(BaseMixin, meta.Base):
    __tablename__ = "tags"

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    name = Column(Unicode(64), nullable=False)