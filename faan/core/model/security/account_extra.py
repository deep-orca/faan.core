from sqlalchemy import Column, Integer, Unicode
from faan.core.model import BaseMixin, meta, XEnum


class _AccountPurchasingType(XEnum):
    """List of types of purchasing systems"""

    def __init__(self):
        XEnum.__init__(self)
        self.WEBMONEY = 1
        self.YANDEXMONEY = 2
        self.PAYPAL = 3
        self.CREDIT_CARD = 4
        self.QIWI = 5

    def get_dict_type(self):
        return {self.WEBMONEY: u'WebMoney',
                self.YANDEXMONEY: u'Yandex money',
                self.PAYPAL: u'PayPal',
                self.CREDIT_CARD: u'CreditCards',
                self.QIWI: u'Qiwi',
                }

    def get_label_by_type(self, label):
        try:
            return self.get_dict_type()[label]
        except KeyError:
            return u'Unknown type'


class _AccountPurchasingState(XEnum):
    """List of states of purchasing systems"""

    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.ACTIVE = 1

    def get_dict_state(self):
        return {self.NONE: u'Passive',
                self.ACTIVE: u'Active',
                }

    def get_label_by_state(self, state):
        try:
            return self.get_dict_state()[state]
        except KeyError:
            return u'Unknown type'


class _AccountCommunicationTypes(XEnum):
    """Methods to connect with an user."""

    def __init__(self):
        XEnum.__init__(self)
        self.SKYPE = 2
        self.PHONE = 3
        self.ICQ = 4
        self.JABBER = 5
        self.MAIL_RU = 6
        self.WHATSAPP = 7
        self.SITE = 8


class AccountExtra(BaseMixin, meta.Base):

    __tablename__ = 'account_extra'

    id = Column(Integer, primary_key=True, autoincrement=True)
    account_id = Column(Integer)
    name = Column(Unicode(127))
    surname = Column(Unicode(127))
    # TODO: Deprecated! Remove this field.
    partner_group_id = Column(Integer)
    company = Column(Unicode(255), default=u'')
    description = Column(Unicode(255), default=u'')


class AccountExtraCommunication(BaseMixin, meta.Base):

    __tablename__ = 'communications'

    id = Column(Integer, primary_key=True, autoincrement=True)
    account_extra_id = Column(Integer, nullable=False)
    type = Column(Integer, nullable=False)
    value = Column(Unicode(255), nullable=False)

    Type = _AccountCommunicationTypes()
    # TODO: deprecate this field.
    Account_communication_types = _AccountCommunicationTypes()


class AccountPurchasingInfo(BaseMixin, meta.Base):

    __tablename__ = 'purchasing_into'

    id = Column(Integer, primary_key=True, autoincrement=True)
    account_extra_id = Column(Integer, nullable=False)
    type = Column(Integer, nullable=False)
    value = Column(Unicode(255), nullable=False)
    state = Column(Integer)

    Type = _AccountPurchasingType()
    State = _AccountPurchasingState()