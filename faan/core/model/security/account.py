from random import sample
from hashlib import sha224
from string import digits, ascii_letters

from sqlalchemy import Column, Integer, Unicode, sql, DECIMAL, DateTime, Float, ForeignKey, BIGINT
from sqlalchemy.orm import relationship
from faan.core.model import meta, XEnum, BaseMixin


class _AccountTypes(XEnum):
    '''Account states map'''
    def __init__(self):
        XEnum.__init__(self)
        # General user types
        self.USER = 1
        self.PARTNER = 2
        # Special system types
        self.MANAGER = 100
        self.FINANCIAL_MANAGER = 120
        self.ADMINISTRATOR = 150
        self.DEVELOPER = 200
        self.GOD = 1000

class _AccountStates(XEnum):
    '''Account states map'''
    def __init__(self):
        XEnum.__init__(self)
        self.NEW = 0
        self.ACTIVE = 1
        self.BANNED = 2
        self.SUSPENDED = 3

class _AccountFlags(XEnum):
    '''Account flags bitmap'''
    def __init__(self):
        XEnum.__init__(self)
        self.UNVERIFIED = 1
        self.TEMPORARY = 1 << 1
        self.PREMIUM = 1 << 2
        self.LOCAL = 1 << 3
        self.FIXED_IP = 1 << 4

class _AccountGroups(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.PUB = 1
        self.ADV = 2
        # TODO: remove this group type
        self.AGENCY = 3

class _AccountPresetsHolder(object): pass

class Account(BaseMixin, meta.Base):
    
    __tablename__ = 'accounts'
    
    id                  = Column(
        Integer,
        # ForeignKey("tickets_reply.account_id"),
        # ForeignKey("tickets.account_id"),
        primary_key = True, autoincrement=True
    )       #@ReservedAssignment
    username            = Column(Unicode(64), unique=True)
    password            = Column(Unicode(64))
    email               = Column(Unicode(255), unique=True)
    permissions         = Column(Integer)
    groups              = Column(Integer)
    type                = Column(Integer)                                               #@ReservedAssignment
    state               = Column(Integer)
    flags               = Column(Integer)
    balance             = Column(DECIMAL)
    date_registration   = Column(DateTime)
    rate                = Column(DECIMAL, default=None)
    request_cost        = Column(DECIMAL, default=None)
    overlay_cost        = Column(DECIMAL, default=None)
    endcard_cost        = Column(DECIMAL, default=None)
    completion_ratio    = Column(DECIMAL, default=None)
    partner_group_id    = Column(BIGINT, nullable=False, default=0)

    # Predefined collections
    Type        = _AccountTypes()
    State       = _AccountStates()
    Flag        = _AccountFlags()
    Preset      = _AccountPresetsHolder()
    Groups      = _AccountGroups()

    # Constants
    MINIMAL_PASSWORD = 6
    MINIMAL_USERNAME = 4

    def __init__(self):
        pass

    @staticmethod
    def PassHash(password):
        """
        Returns a SHA224 makeup of given string password for storing it in DB
        """
        return sha224(password).hexdigest()

    @staticmethod
    def NewCode():
        """
        Generates an unique login-code
        """
        return ''.join([sample(digits, 1)[0] for x in range(8)])     #@UnusedVariable

    @staticmethod
    def random_password():
        return ''.join([sample(digits + ascii_letters, 1)[0] for x in range(16)])

# Post-install 
Account.Preset.NONE_TYPE   = (Account.type == 0)
Account.Preset.BANNED      = (Account.state == Account.State.BANNED)
Account.Preset.SUSPENDED   = (Account.state == Account.State.SUSPENDED)
Account.Preset.ACTIVE      = (Account.state == Account.State.ACTIVE)
Account.Preset.LOGIN_USER  = sql.and_(sql.not_(Account.Preset.NONE_TYPE), Account.Preset.ACTIVE)
Account.Preset.BY_CODE     = lambda code: Account.code == code
Account.Preset.BY_PASS     = lambda u, p: sql.and_((Account.username == unicode(u)) , (Account.password == Account.PassHash(p)))

