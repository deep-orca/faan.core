from sqlalchemy import Column, Integer, Unicode, DateTime
from faan.core.model import meta, BaseMixin


class Comment(BaseMixin, meta.Base):

    __tablename__ = 'comments'

    id = Column(Integer, primary_key=True, autoincrement=True)
    date = Column(DateTime, nullable=False)
    comment_producer_id = Column(Integer)
    comment_consumer_id = Column(Integer, nullable=False)
    text = Column(Unicode(200))
