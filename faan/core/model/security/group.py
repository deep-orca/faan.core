from sqlalchemy import Column, Integer, Unicode
from faan.core.model import meta, PGArray, XEnum, BaseMixin

class Group(meta.Base, BaseMixin):
    
    __tablename__ = 'groups'
    
    id          = Column(Integer, primary_key=True, autoincrement=True)     #@ReservedAssignment
    name        = Column(Unicode(64))
    description = Column(Unicode(255))
    permissions = Column(PGArray(Integer))
    action      = Column(Integer)
    ring        = Column(Integer)
    site        = Column(Integer)
    controller  = Column(Integer)
    method      = Column(Integer)
    type        = Column(Integer)                                           #@ReservedAssignment
    state       = Column(Integer)
    flags       = Column(Integer)
   
    Type    = XEnum()
    State   = XEnum()
    Flag    = XEnum()
