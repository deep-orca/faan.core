from sqlalchemy import Column, Integer, Unicode, DateTime
from faan.core.model import meta, BaseMixin, XEnum


class _MessageStatus(XEnum):
    """Status of messages."""

    def __init__(self):
        XEnum.__init__(self)
        self.NEW = 1
        self.VIEWED = 2


class Message(BaseMixin, meta.Base):

    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True, autoincrement=True)
    date = Column(DateTime, nullable=False)
    message_producer_id = Column(Integer)
    message_consumer_id = Column(Integer, nullable=False)
    status = Column(Integer)
    text = Column(Unicode(255))

    Status = _MessageStatus()
