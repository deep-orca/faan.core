from decimal import Decimal
from sqlalchemy import Column, Integer, Unicode, DECIMAL
from faan.core.model import meta, BaseMixin



class PartnerGroup(BaseMixin, meta.Base):

    __tablename__ = 'partner_groups'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(Unicode(64))
    rate = Column(DECIMAL, nullable=False, default=Decimal(0))
    request_cost = Column(DECIMAL, nullable=False, default=Decimal(0))
    overlay_cost = Column(DECIMAL, nullable=False, default=Decimal(0))
    endcard_cost = Column(DECIMAL, nullable=False, default=Decimal(0))
    completion_ratio = Column(DECIMAL, nullable=False, default=Decimal(0))
    account_type = Column(Integer)

    DEFAULT_GROUP = 0



