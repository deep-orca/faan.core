from faan.core.model import meta, BaseMixin
from sqlalchemy import Column, Integer, Unicode

class Permission(BaseMixin, meta.Base):
    
    __tablename__ = 'permissions'
    
    id          = Column(Integer, primary_key = True)   #@ReservedAssignment
    name        = Column(Unicode(64))
    description = Column(Unicode(255))
    
    action      = Column(Integer)                       # allow / deny
    
    ring        = Column(Integer)
    site        = Column(Integer)
    controller  = Column(Integer)
    method      = Column(Integer)
    specific    = Column(Integer)    
    type        = Column(Integer)                       #@ReservedAssignment
    state       = Column(Integer)
    flags       = Column(Integer)  
