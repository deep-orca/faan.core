
from faan.core.model import meta, PGArray
from faan.core.X import XEnum
from sqlalchemy import Column, Integer, Unicode, text
from faan.core.model.basemixin import BaseMixin

class _Category_Types(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.CAMPAIGN = 0
        self.APP = 1
        self.GAME = 2


class _Category_Flags(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.DISABLED = 1
        self.INVISIBLE = 1 << 1


class _Category_States(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.ACTIVE = 1
        self.INVISIBLE = 2


class _Category_Platforms(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.NONE = 0
        self.IOS = 1
        self.ANDROID = 2
        self.WINDOWS = 3


class Category(BaseMixin, meta.Base):

    __tablename__ = 'categories'
    
    id      = Column(Integer, primary_key=True, autoincrement=True)     #@ReservedAssignment
    name    = Column(Unicode(64))
    type    = Column(Integer)                                           #@ReservedAssignment
    state   = Column(Integer)
    flag   = Column(Integer)
    platform = Column(Integer)
    related_categories = Column(PGArray(Integer),  nullable=False, default=[],  server_default=text("ARRAY[]::INTEGER[]"))
    
    Type = _Category_Types()
    Flag = _Category_Flags()
    State = _Category_States()
    Platform = _Category_Platforms()