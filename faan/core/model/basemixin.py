from faan.core.model.meta import Session


class BaseMixin(object):
    @classmethod
    def Query(cls):
        return Session.query(cls)
    
    @classmethod
    def Get(cls, *args):
        return Session.query(cls).get(args) # IGNORE:E1101

    @classmethod
    def Key(cls, *args):
        o = cls.Get(*args) 
        if o is None:
            raise KeyError("NO OBJECT FOUND")
        
        return o

    @classmethod
    def Filter(cls, *args, **kwargs):
        ret = Session.query(cls)
        for arg in args:
            ret = ret.filter(arg)
        for key, val in kwargs.iteritems():
            if key in cls.__dict__:
                ret = ret.filter(cls.__dict__[key] == val)
        return ret

    @classmethod
    def Count(cls, *args, **kwargs):
        return cls.Filter(*args, **kwargs).count()

    @classmethod
    def GetLocked(cls, *args):
        return Session.query(cls).with_lockmode('update').get(args)

    @classmethod
    def One(cls, *args, **kwargs):
        return cls.Filter(*args, **kwargs).one()

    @classmethod
    def Single(cls, *args, **kwargs):
        allobjs = cls.Filter(*args, **kwargs).limit(2).all()
        return allobjs[0] if len(allobjs) == 1 else None

    @classmethod
    def All(cls, *args, **kwargs):
        return cls.Filter(*args, **kwargs).all()

    @classmethod
    def Default(cls, *args, **kwargs):
        o = cls()
        
        for n, c in cls.__table__.columns.items():
            if c.default is not None:
                if hasattr(c.default.arg, '__call__'):
                    setattr(o, n, c.default.arg(None))
                else:
                    setattr(o, n, c.default.arg)
        return o
    
    @classmethod
    def Update(cls, fields_update_dict, *filters, **kwfilters):                        # USE WITH CAUTION: MASS UPDATES!
        if not filters and not kwfilters:
            raise RuntimeError("at least one filter/kwfilter must be specified, or None used as second arg")
        
        if filters and filters[0] == None:
            filters = filters[1:]

        return cls.Filter(*filters, **kwfilters).update(fields_update_dict)
            


class StatMixin(object):
    @classmethod
    def StatCount(cls, stat):
        if not stat:
            raise ValueError('stat object omitted')

        if cls.CountUpdate(stat) : return
        if cls.CountInsert(stat) : return
        if cls.CountUpdate(stat) : return

        raise ValueError('stat count failed : %s' % unicode(stat))

    @classmethod
    def CountInsert(cls, st):
        try:
            Session.add(st)
            Session.flush()
            Session.commit()
            return True
        except:
            print "EXCEPTION : CountInsert"
            Session.rollback()
            return False
 
    @classmethod
    def CountUpdate(cls, st):
        try:
            key = {}
            upd = {}
            
            for n, c in cls.__table__.columns.items():
                if c.primary_key:
                    key[n] = getattr(st, n)
                else:
                    upd[n] = getattr(cls, n) + getattr(st, n)
                
                
            row_count = cls.Update(upd, **key)
            Session.flush()
            Session.commit()
            return row_count == 1
        except:
            print "EXCEPTION : CountUpdate"
            Session.rollback()
            return False
 
    @classmethod
    def PreQuery(cls):
        from sqlalchemy.sql.functions import sum as sqlsum
        
        l = []
        
        for n, c in cls.__table__.columns.items():
            if not c.primary_key:
                l.append(sqlsum(getattr(cls, n)).label(n)) 
                
        return Session.query(*l)










