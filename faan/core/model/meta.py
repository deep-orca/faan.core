from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm.scoping import ScopedSession


class x_scoped_session(ScopedSession):
    '''Attributes passed in constructor.
    *@DynamicAttrs*
    '''    
    pass


#Session = x_scoped_session(sessionmaker(autocommit=True, autoflush=False)) # TODO: check if its ok
Session = x_scoped_session(sessionmaker())
Base = declarative_base()


__all__ = ['Session']

def init_db():
    Base.metadata.create_all()
    

pps = { 0 : u'developer:ahYaeci0aeph@core.faan.com/tgcore'
      , 1 : u'developer:jeejingae@localhost/core'
      , 2 : u'developer:jeejingae@88.208.57.57/core' 
      }


class SessionFactory(object):

    def _init_session(self, pp, dev_local=False):
        from sqlalchemy.engine import create_engine
        
        host = 'mediafabric:ADADADADADADADADADAD@dev.faan.com/tg' if dev_local else pps[pp]
        return scoped_session(sessionmaker(bind=create_engine('postgresql://%s' % host)))
        
    def tg_session(self):
        return self._init_session(0)

    def nz_session(self):
        return self._init_session(2)
    
    def get_session(self):
        import socket
        
        return self.tg_session() if 'DTG1074' in socket.gethostname() else self.nz_session()
        
from sqlalchemy.engine import create_engine
#command line purposes only
def init_session(dev_local=False, pp=0):
    host = 'mediafabric:ADADADADADADADADADAD@dev.faan.com/tg' if dev_local else pps[pp]
    return scoped_session(sessionmaker(bind=create_engine('postgresql://%s' % host)))

