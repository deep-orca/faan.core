# -*- coding: utf-8 -*-
import urllib
import redis

hash_salt = 'eiv0EiPheish6ot7ohtae8lae0li#sahH}isauN?ahv3icae8kaazae1Ahh2kihoh6lae4Uph;oeph6uex>ei3ohch?ahv2ieciChaekieB9ahX5uKeeghahdaighah}'

def urldecode(url):
    res = {}
    for item in url.split('&'):
        key_val = item.split('=')
        res[urllib.unquote_plus(key_val[0])] = urllib.unquote_plus(key_val[1])
    return res

class XRedis(redis.StrictRedis):
    """
    StrictRedis wrapper
    Change def param values in __init__ method for properly redis setup
    """
    def __init__(self, host='localhost', port=6379, db=0, password=None,
                 socket_timeout=None, connection_pool=None, charset='utf-8',
                 errors='strict', unix_socket_path=None):
        super(XRedis, self).__init__(host, port, db, password, socket_timeout,
            connection_pool, charset, errors, unix_socket_path)
