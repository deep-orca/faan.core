from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.mutable import Mutable
from sqlalchemy.ext.declarative import declared_attr


def PolyID(t, platform, placement):
    class MyMixin(object):
        
        PLATFORM  = platform
        PLACEMENT = placement
        TYPE      = t
        
        POLY = PLATFORM * 10000 + PLACEMENT * 1000 + TYPE   

        @declared_attr
        def __mapper_args__(self): # cls actually
            return { 'polymorphic_identity' : self.POLY }
    
    return MyMixin


class MutableManually(Mutable, dict):
    @classmethod
    def coerce(cls, key, value):
        "Convert plain dictionaries to MutableDict."

        if isinstance(value, MutableManually):
            return value

        if isinstance(value, dict):
            return MutableManually(value)

        return Mutable.coerce(key, value)               # this call will raise ValueError



def JSONField(n, t):
    
    def getter(self):
        return self.parameters.get(n)
    
    def setter(self, value):
        self.parameters[n] = value
        self.parameters.changed()
        
    def deleter(self):
        del self.parameters[n]
        self.parameters.changed()        
    
    def expr(cls):
        return cls.parameters.cast(t)
    
    return hybrid_property(getter, setter, deleter, expr)


