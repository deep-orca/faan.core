'''

'''

class XEnumIterator(object):
    def __init__(self, obj):
        self.obj = obj
        self.index = 0

    def __iter__(self):
        return None

    def next(self):
        if not '_declared_pairs'  in self.obj.__dict__:
            raise StopIteration

        if self.index >= len(self.obj.__dict__['_declared_pairs']):
            raise StopIteration

        name = self.obj.__dict__['_declared_pairs'][self.index]
        self.index = self.index + 1
        return self.obj.__dict__[name]

class XDictPreIter(object):
    def __init__(self, obj):
        self.obj = obj

    def __iter__(self):
        return XDictIterator(self.obj)


class XDictIterator(object):
    def __init__(self, obj):
        self.obj = obj
        self.index = 0

    def __iter__(self):
        return None

    def next(self):
        if not '_declared_pairs'  in self.obj.__dict__:
            raise StopIteration

        if self.index >= len(self.obj.__dict__['_declared_pairs']):
            raise StopIteration

        name = self.obj.__dict__['_declared_pairs'][self.index]
        self.index = self.index + 1
        return (name, self.obj.__dict__[name])

class XEnum(object):
    def _init_pairs(self, **kwargs):
        if not '_declared_pairs'  in self.__dict__:
            self.__dict__['_declared_pairs'] = []
        if not '_reversed_pairs'  in self.__dict__:
            self.__dict__['_reversed_pairs'] = {}
        
        for name, value in kwargs.iteritems():
            self._add_value(name, value)
    
    def _add_value(self, name, value):
        self.__dict__[name] = value
        try:
            self.__dict__['_reversed_pairs'].update([(value, name)])
        except TypeError:
            pass

        if not name in self.__dict__['_declared_pairs']:
            self.__dict__['_declared_pairs'].append(name)
                
    def __init__(self, **kwargs):
        self._init_pairs(**kwargs)


    def __setattr__(self, name, value):
        self._init_pairs()
        self._add_value(name, value)
       

    def __getattr__(self, name):
        return self.__dict__[name]

    def __getitem__(self, index):
        return self.__dict__[index]

    def __iter__(self):
        return XEnumIterator(self)

    def iteritems(self):
        return XDictPreIter(self)

    def keys(self):
        return self.__dict__['_reversed_pairs'].values()

    def values(self):
        return self.__dict__['_reversed_pairs'].keys()

    def inverse(self):
        return self.__dict__['_reversed_pairs']

    def __contains__(self, key):
        return key in self.__dict__['_declared_pairs']

    def __len__(self):
        if not '_declared_pairs'  in self.__dict__:
            return 0
        else:
            return len(self.__dict__['_declared_pairs'])
