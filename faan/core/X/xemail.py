# -*- coding: utf-8 -*-

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from faan.core.X.xflask import capp

SMTP_SERVER = 'smtp.gmail.com'
SMTP_PORT = 587

sender = ''
password = ''
email_admin = ''

_html = u"""
<html>
  <head>{header}</head>
  <body>
    <p>{hello}<br>
       {body}
    </p>
  </body>
</html>
"""


def init_email(app):
    global sender, password, email_admin
    sender = app.config.get('EMAIL_SENDER', '')
    password = app.config.get('EMAIL_PASSWORD', '')
    email_admin = app.config.get('EMAIL_ADMIN', '')


def send_email(recipient, subject, body):

    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['To'] = recipient
    msg['From'] = sender

    part = MIMEText(unicode(body), "html", _charset="utf-8")

    msg.attach(part)

    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    try:
        server.starttls()
        server.login(sender, password)
        server.sendmail(sender, recipient, msg.as_string().encode('ascii'))
    except smtplib.SMTPAuthenticationError:
        err_msg = 'EXCEPTION IN send_email. ' \
                  'Username and Password not accepted(%s, %s)' % \
                  (sender, password)
        capp.logger.exception(err_msg)
    finally:
        server.quit()


def send_email_notify_admin(account_email, account_id):
    subject = u"Vidiger. Заявка на регистрацию."
    hello = ""
    header = ""
    body = u"""
<img src="http://vidiger.com/static/home_assets/images/main-logo.png" />

<h3>Здравствуйте!</h3>

Пользователь <a href="http://admin.vidiger.com/profile/{account_id}/">{user}</a> подал заявку на регистрацию в сети мобильной рекламы Vidiger.
<br><br>
С уважением,<br>
команда Vidiger.
    """\
    .replace(u'{user}', account_email)\
    .replace(u'{account_id}', str(account_id))

    html = _html\
        .replace(u'{header}', header)\
        .replace(u'{hello}', hello)\
        .replace(u'{body}', body)\

    send_email(email_admin, subject, html)


def send_email_registration(recipient):
    subject = u"Vidiger. Заявка на регистрацию."
    hello = ""
    header = ""
    body = u"""
<img src="http://vidiger.com/static/home_assets/images/main-logo.png" />

<h3>Здравствуйте!</h3>

Вы подали заявку на регистрацию в сети мобильной рекламы Vidiger. Каждая заявка проходит персональное рассмотрение. Наши менеджеры свяжутся с Вами по одному из контактов, оставленных Вами.
<br><br>
Если у Вас есть вопросы - обращайтесь по одному из <a href="http://vidiger.com/contacts/">контактов</a>.
<br><br>
С уважением,<br>
команда Vidiger.
    """
    html = _html\
        .replace(u'{header}', header)\
        .replace(u'{hello}', hello)\
        .replace(u'{body}', body)

    send_email(recipient, subject, html)


def send_email_approve(recipient, username, passwd):
    subject = u"Vidiger. Подтвержение регистрации."
    hello = ""
    header = ""
    body = u"""
<img src="http://vidiger.com/static/home_assets/images/main-logo.png" />

<h3>Здравствуйте!</h3>

Ваша заявка на регистрацию в сети мобильной рекламы Vidiger была подтверждена.<br>
Чтобы войти в Ваш аккаунт, используйте следующие данные:
<br><br>
<b>Логин</b>: {username}<br>
<b>Пароль</b>: {password}<br>
<br><br>
Если у Вас есть вопросы или нужна помощь по работе с аккаунтом - обращайтесь по одному из <a href=”http://vidiger.com/contacts/”>контактов</a>.
<br><br>
С уважением,<br>
команда Vidiger.
    """

    html = _html\
        .replace(u'{header}', header)\
        .replace(u'{hello}', hello)\
        .replace(u'{body}', body)\
        .replace(u'{username}', username)\
        .replace(u'{password}', passwd)\

    send_email(recipient, subject, html)


def send_email_block(recipient):
    subject = u"Vidiger. Блокировка аккаунта."
    hello = ""
    header = ""
    body = u"""
<img src="http://vidiger.com/static/home_assets/images/main-logo.png" />

<h3>Здравствуйте!</h3>

Вы подавали заявку на регистрацию в сети Vidiger, однако, к сожалению, в данный момент мы не готовы сотрудничать с Вами.
<br><br>
Если у Вас есть вопросы - обращайтесь по одному из <a href=”http://vidiger.com/contacts/”>контактов</a>.
<br><br>
С уважением,<br>
команда Vidiger.
    """

    html = _html\
        .replace(u'{header}', header)\
        .replace(u'{hello}', hello)\
        .replace(u'{body}', body)

    send_email(recipient, subject, html)


def send_email_active(recipient, username):
    subject = u"Vidiger. Аккаунт снова активен."
    hello = ""
    header = ""
    body = u"""
<img src="http://vidiger.com/static/home_assets/images/main-logo.png" />

<h3>Здравствуйте!</h3>

Ваш аккаунт <b>{username}</b> снова активен.
<br><br>
Если у Вас есть вопросы - обращайтесь по одному из <a href=”http://vidiger.com/contacts/”>контактов</a>.
<br><br>
С уважением,<br>
команда Vidiger.
    """

    html = _html\
        .replace(u'{header}', header)\
        .replace(u'{hello}', hello)\
        .replace(u'{body}', body)\
        .replace(u'{username}', username)

    send_email(recipient, subject, html)