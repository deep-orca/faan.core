# coding=utf-8

__author__ = 'limeschnaps'


class Permissions(object):
    '''
    *@DynamicAttrs*
    '''
    
    def __init__(self, **predicates):
        for for_, predicate in predicates.iteritems():
            setattr(self, for_, predicate)
    

