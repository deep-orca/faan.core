# coding=utf-8
from flask import request
from faan.core.model.meta import Session
from faan.core.model.security import Account

__author__ = 'limeschnaps'


def is_active(item):
    if item.sub_items and item.url != "/":
        return request.path.startswith(item.url) or any(map(is_active, item.sub_items))

    return request.path == item.url


def get_account():
    account_id = request.environ.get("REMOTE_USER", 0)
    if account_id:
        return Session.query(Account).get(account_id)


class MenuItem(object):
    def __init__(self, name, for_types=None, for_groups=None, url="#", icon="", extra_css=None, sub_items=None,
                 extra_html=None):
        self.name = name
        self.url = url
        self.icon = u"<i class='fa fa-{icon}'></i>".format(icon=icon) if icon else u""
        self.extra_css = u" ".join(extra_css) if extra_css else u""
        self.sub_items = sub_items

        self.for_types = for_types or []
        self.for_groups = for_groups or []

        self.extra_html = extra_html


    def get_extra_html(self):
        if self.extra_html:
            if callable(self.extra_html):
                return self.extra_html()
            else:
                return self.extra_html

    def render(self, account):
        _sub_items = []
        if self.sub_items:
            for item in self.sub_items:
                _sub_item = item.render(account) or None
                if _sub_item:
                    _sub_items.append(_sub_item)

        sub_menu = u"<ul class='sub-menu'>{items}</ul>".format(items=u"\n".join(_sub_items)) if _sub_items else u""

        if (account.type in self.for_types or account.groups in self.for_groups) or \
                (not self.for_groups and not self.for_types):
            return u"<li class='{is_active} {extra_css}'>" \
                   u"<a href='{url}'>" \
                   u"{icon}" \
                   u"<span class='title'> {name} {extra_html}</span>" \
                   u"</a>" \
                   u"{sub_menu}" \
                   u"</li>".format(
                       name=self.name,
                       url=self.url,
                       icon=self.icon,
                       sub_menu=sub_menu,
                       extra_css=self.extra_css,
                       is_active=u"active" if is_active(self) else u"",
                       extra_html=self.get_extra_html() or u""
                   )
        else:
            return ""


class Menu(object):
    def __init__(self, items):
        self.items = items

    def render(self):
        account = get_account()
        if account:
            _items = []
            for item in self.items:
                if isinstance(item, MenuItem):
                    _items.append(item.render(account))

            return "\n".join(_items)
        else:
            return ""