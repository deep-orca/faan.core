# coding=utf-8
import traceback

__author__ = 'limeschnaps'

import os
from hashlib import md5
from PIL import Image, ImageOps
from faan.core.X.xflask import capp


class Thumbnail(object):
    def __init__(self, size, url):
        self.width, self.height = size
        self.url = url


class ThumbnailHelper(object):
    def __init__(self, *args, **kwargs):
        self.thumbnail_folder = os.path.join(capp.config['GLOBAL_FOLDER'], 'thumbnails')
        self.static_folder = os.path.join('/', *(capp.config['GLOBAL_FOLDER'].split('/')[:-1]))

    def __call__(self, image_url, size, crop=None):
        image_path = os.path.join(self.static_folder, *(image_url.split('/')))
        image_hash = md5('%s:%s:%s:%s' % (image_path, os.stat(image_path).st_size, os.stat(image_path).st_mtime, size)).hexdigest()

        if not os.path.isfile("%s.jpg" % os.path.join(self.thumbnail_folder, image_hash)):
            image = Image.open(image_path)
            iw, ih = image.size
            if iw < size[0] or ih < size[1]:
                thumb = image
                thumb.thumbnail(size, Image.ANTIALIAS)

                thumb.save("%s.jpg" % os.path.join(self.thumbnail_folder, image_hash))
            else:
                thumb = ImageOps.fit(image, size, Image.ANTIALIAS)
                thumb.save("%s.jpg" % os.path.join(self.thumbnail_folder, image_hash))

            print thumb.size
            # ImageOps.
            # image.save("%s.jpg" % os.path.join(self.thumbnail_folder, image_hash))

        thumb_path = "%s.jpg" % os.path.join(self.thumbnail_folder, image_hash)
        thumb = Image.open(thumb_path)
        # return "%s.jpg" % os.path.join('/', *(self.thumbnail_folder.split('/')[-2:] + [image_hash]))
        return Thumbnail(thumb.size,
                         "%s.jpg" % os.path.join('/', *(self.thumbnail_folder.split('/')[-2:] + [image_hash])))

thumbnail = ThumbnailHelper()