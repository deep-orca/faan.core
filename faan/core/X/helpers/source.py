# coding=utf-8

__author__ = 'limeschnaps'
from faan.core.model.pub.application import Application
from faan.core.model.pub.unit import Unit

from faan.core.model.pub.source import (
    Source,
    VidigerIOSInterstitialSource,
    VidigerAndroidInterstitialSource,
    VidigerWinPhoneInterstitialSource,
    VidigerMobileWebInterstitialSource,
    VidigerIOSBannerSource,
    VidigerAndroidBannerSource,
    VidigerWinPhoneBannerSource,
    VidigerMobileWebBannerSource,
    AdMobIOSBannerSource,
    AdMobAndroidBannerSource,
    AdMobAndroidInterstitialSource,
    AdMobIOSInterstitialSource
)

# from faan.web.forms.publisher import (
#     SourceBannerVidigerForm,
#     SourceInterstitialVidigerForm,
#     SourceBannerAdMobForm,
#     SourceInterstitialAdMobForm
# )


class SourceHelperFormObject(object):
    def __init__(self, source_type, unit_type, form_cls):
        self.source_type = source_type
        self.unit_type = unit_type
        self.form_cls = form_cls

    def __call__(self):
        return {
            (self.source_type, self.unit_type): self.form_cls
        }


class SourceHelper(object):
    def __init__(self, form_mapping, debug=False):
        for form_obj in form_mapping:
            self.form_mapping.update(form_obj())

        if debug: print self.form_mapping

    form_mapping = {}
    class_mapping = {
        (Source.Type.VIDIGER, Application.Os.IOS, Unit.Type.INTERSTITIAL): VidigerIOSInterstitialSource,
        (Source.Type.VIDIGER, Application.Os.ANDROID, Unit.Type.INTERSTITIAL): VidigerAndroidInterstitialSource,
        (Source.Type.VIDIGER, Application.Os.WINPHONE, Unit.Type.INTERSTITIAL): VidigerWinPhoneInterstitialSource,
        (Source.Type.VIDIGER, Application.Os.MOBILE_WEB, Unit.Type.INTERSTITIAL): VidigerMobileWebInterstitialSource,
        (Source.Type.VIDIGER, Application.Os.IOS, Unit.Type.BANNER): VidigerIOSBannerSource,
        (Source.Type.VIDIGER, Application.Os.ANDROID, Unit.Type.BANNER): VidigerAndroidBannerSource,
        (Source.Type.VIDIGER, Application.Os.WINPHONE, Unit.Type.BANNER): VidigerWinPhoneBannerSource,
        (Source.Type.VIDIGER, Application.Os.MOBILE_WEB, Unit.Type.BANNER): VidigerMobileWebBannerSource,
        (Source.Type.ADMOB, Application.Os.IOS, Unit.Type.INTERSTITIAL): AdMobIOSInterstitialSource,
        (Source.Type.ADMOB, Application.Os.ANDROID, Unit.Type.INTERSTITIAL): AdMobAndroidInterstitialSource,
        (Source.Type.ADMOB, Application.Os.IOS, Unit.Type.BANNER): AdMobIOSBannerSource,
        (Source.Type.ADMOB, Application.Os.ANDROID, Unit.Type.BANNER): AdMobAndroidBannerSource,
    }

    def get(self, *args):
        return self.class_mapping.get(args), self.form_mapping.get((args[0], args[2]))

    # form_mapping = {
    #     (Source.Type.VIDIGER, Unit.Type.BANNER): SourceBannerVidigerForm,
    #     (Source.Type.VIDIGER, Unit.Type.INTERSTITIAL): SourceInterstitialVidigerForm,
    #     (Source.Type.ADMOB, Unit.Type.BANNER): SourceBannerAdMobForm,
    #     (Source.Type.ADMOB, Unit.Type.INTERSTITIAL): SourceInterstitialAdMobForm,
    # }