from faan.core.model import meta
from sqlalchemy.engine import create_engine

class SessionRemover(object):
    def __init__(self, app, global_config, conn_string = None, **kwargs):
        self.app = app
        meta.Base.metadata.bind = create_engine(conn_string)
    
    def __call__(self, environ, start_response):
        try:
            # TODO: add session.echo for current request 
            return self.app(environ, start_response)
        finally:
            meta.Session.remove()
