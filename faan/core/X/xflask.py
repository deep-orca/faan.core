import os
import imp
import inspect
from flask import Flask, request, redirect, g, current_app as capp #@UnusedImport

def scan_folder_recurse(folder, excl_names=['__']):
    """ Recurse search for PY files in given folder """                                                                                                                                   
    all_files = []                                                                                                                                                                      
    for root, d, files in os.walk(folder):                                              #@UnusedVariable
        filelist = [os.path.join(root, fi) for fi in files if fi.endswith('.py')
                    and not any(fi.startswith(prefix) for prefix in excl_names)]
        all_files += filelist
    return all_files


def getCurrentIP():
    if "X-Forwarded-For" in request.headers:
        h = request.headers['X-Forwarded-For']
    else:
        h = request.environ['REMOTE_ADDR']
        
    return h.split(',')[0]


def Controller(*args):
    if not inspect.isclass(args[0]):
        def d(cls):
            setattr(cls, "XFLASK_CONTROLLER", True)
            setattr(cls, "XFLASK_CONTROLLER_PATH", args[0])
            return cls
    
        return d
    else:
        cls = args[0]
        setattr(cls, "XFLASK_CONTROLLER", True)
        setattr(cls, "XFLASK_CONTROLLER_PATH", "")
        return cls
        
        
def Route(path = "", **kwargs):
    def d(f):
        paths = []
        if hasattr(f, "XFLASK_METHOD_PATH"):
            paths = getattr(f, "XFLASK_METHOD_PATH")
        
        paths.append((path, kwargs))
        
        setattr(f, "XFLASK_METHOD_PATH", paths)
        return f
    return d

def Error(ex = ""):
    def d(f):
        exs = []
        if hasattr(f, "XFLASK_ERROR_EXCEPTIONS"):
            exs = getattr(f, "XFLASK_ERROR_EXCEPTIONS")
        
        exs.append(ex)
        
        setattr(f, "XFLASK_ERROR_EXCEPTIONS", exs)
        return f
    return d


class XFlask(Flask):
    def __init__(self, *args, **kwargs):
        Flask.__init__(self, *args, **kwargs)
        
    def find_controllers(self, package_root_path, package_root_name):
        all_files = scan_folder_recurse(package_root_path)
        for f in all_files:
            pkg = f[len(package_root_path):]
            pkg = pkg.strip("/")[:-3]
            
            if len(pkg):
                pkg = package_root_name + "." + pkg
            
            controller = imp.load_source(pkg, f)
            my_list = dir(controller)
            for element in my_list:
                if element.startswith("__"):
                    continue
                
                cls = getattr(controller, element)
                if not inspect.isclass(cls):
                    continue
                
                if not cls.__module__.startswith(package_root_name):
                    continue
                
                if not hasattr(cls, "XFLASK_CONTROLLER"):
                    continue
                
                cpath = getattr(cls, "XFLASK_CONTROLLER_PATH")
                ctrl = cls()
                methods = dir(ctrl)
                for m in methods:
                    method = getattr(ctrl, m)
                    if hasattr(method, "XFLASK_METHOD_PATH"):
                        mpaths = getattr(method, "XFLASK_METHOD_PATH")
                        for mpath in mpaths:
                            xf = cls.__dict__[method.__name__].__get__(ctrl, cls)
                            xendpoint = pkg + "." + cls.__name__ + "." + xf.__name__
                            mpath[1]['endpoint'] = xendpoint
                            self.route(cpath + mpath[0], **mpath[1])(xf)

                    if hasattr(method, "XFLASK_ERROR_EXCEPTIONS"):
                        exs = getattr(method, "XFLASK_ERROR_EXCEPTIONS")
                        for ex in exs:
                            self._register_error_handler(None, ex, cls.__dict__[method.__name__].__get__(ctrl, cls))




