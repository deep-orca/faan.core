
from paste.registry import StackedObjectProxy
from faan.core.model.domain import Domain

CurrentSite = StackedObjectProxy(name = "SiteID Object")

from faan.core.model.site import Site


class SiteCurrentUrlback(object):
    def __get__(self, instance, owner):
        return str(instance.ID) + ":" + instance._ub


class SiteCurrentID(object):
    def __get__(self, instance, owner):
        return instance.Obj.id

class SiteCurrentDomain(object):
    def __get__(self, instance, owner):
        return instance.Obj.domain

class SiteCurrentObj(object):
    def __get__(self, instance, owner):
        obj = Domain.One(name=instance._domain)
        return Site.One(id=obj.site)
    
class SiteStatic(object):
    def __get__(self, instance, owner):
        return instance.Obj.static

class CSiteCurrent(object):
    ID = SiteCurrentID()
    Domain = SiteCurrentDomain()
    UrlBack = SiteCurrentUrlback()
    Obj = SiteCurrentObj()
#    Static = SiteStatic()

    def __init__(self, environ):
        self.parker = []
        self._domain = environ.get('HTTP_HOST', '')
        if self._domain.startswith('www.'):
            self._domain = self._domain[4:]
            
        self._ub = ""
        if 'SCRIPT_NAME' in environ:
            self._ub += environ['SCRIPT_NAME']
        if 'PATH_INFO' in environ:
            self._ub += environ.get('PATH_INFO', '') or "/"
        if 'QUERY_STRING' in environ:
            self._ub += "?" + environ['QUERY_STRING']
    
    @property    
    def current_domain(self):
        return self._domain
            
    @current_domain.getter    
    def current_domain(self):
        return self._domain
    
class sitecurrent_middleware(object):
    '''
    SiteServer Middleware object
    '''
    def __init__(self, app, *args, **kwargs):
        self.app = app

    def __call__(self, environ, start_response):
        SiteCurrentObj = CSiteCurrent(environ)

        if environ.get('paste.registry') and environ['paste.registry'].reglist:
            environ['paste.registry'].register(CurrentSite, SiteCurrentObj)

        return self.app(environ, start_response)


